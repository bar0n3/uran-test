var development = require('./dev');
var staging = require('./stag');

var config = {
	dev: development,
	stag: staging
}

module.exports = config;