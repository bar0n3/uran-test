var path = require('path');
var express = require('express');
var bodyParser = require('body-parser');
var mongoose = require('mongoose');
var conf = require('./config');

var routes = require('./routes/routes');

var app = express();

app.set('view engine', 'pug');

// static folders
app.use("/assets", express.static(path.join(__dirname, 'assets')));
app.use("/node_modules", express.static(path.join(__dirname, 'node_modules')));

// development only
if ('development' == app.get('env')) {
    mongoose.connect(conf.dev.db_connection);
    var db = mongoose.connection;
    db.on('error', console.error.bind(console, 'MongoDB connection error:'));
}

// routes 
app.use('/', routes);

app.listen(3000);
