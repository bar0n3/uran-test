# README #

1. Exclude landingdb.zip archive
2. Open terminal window outside landingdb folder location with admin privileges
3. If you don't have MongoDB installed, then it's right time to do it
4. Check if you can see landingdb folder when checking files listing (Type command: dir or ls)
5. Restore backup with command: 
	mongorestore --port 27017 landingdb/
	If you have issues, check where your mongodb instance was installed. 
	Go there and run this command via terminal, 
	don't forget to mention the full path to landingdb folder.
6. Start MongoDB service (On Windows: mongod --port 27017; net start MongoDB; )
7. Install dependencies and run project: npm i && nodemon
