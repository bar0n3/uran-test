var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var schema = new Schema({
    lang: String,
    title: String,
    header: {
        title: String,
        subtitle: String,
        menu: [String]
    },
    items: {
        about: {
            title: String,
            subtitle: String,
            img: String
        },
        services: {
            title: String,
            subtitle: String,
            img: String
        },
        contact: {
            title: String,
            subtitle: String,
            img: String
        }
    },
    footer: {
        menu: [String],
        message: String,
        copyright: String
    },
    socials: [{
    		name: String,
    		url: String,
    		icon: String
    }]
}, {collection:'landing'});


var landingModel = module.exports = mongoose.model('landing', schema);

// get landing data

module.exports.getLang = function(lang, callback) {
	landingModel.findOne({lang: lang}, callback);
}