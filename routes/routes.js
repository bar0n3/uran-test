var express = require('express');
var router = express.Router();
var model = require('../models/landing');

function defineLang(req, res, next) {
    var newpath = req.path.replace('/','');
    req.pageLang = ['ru', 'en'].indexOf(newpath) > -1 ? newpath : 'en';
    next();
}
function renderModel(req, res, next) {
    model.getLang(req.pageLang, function(err, data) {    
        if (err) throw err;
        if (data) {
            res.render('index', data);
        }
    });
}
router.get('/', defineLang, renderModel);

router.get('/ru', defineLang, renderModel);

router.get('/en', defineLang, renderModel);

module.exports = router;